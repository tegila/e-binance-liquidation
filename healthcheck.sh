#!/bin/bash
LOGFILE="/usr/src/binance/spot/output.log"
#LOGFILE="output.log"

last_line=( $(tail -1 ${LOGFILE} ) )

date_from=$(date --date="${last_line[0]} ${last_line[1]}" +%s)
echo $date_from

date_to=$(date +%s)
echo $date_to

#difference in minutes (60)
date_diff=$(( ($date_to - $date_from)/(60) ))
echo $date_diff

if [ "$date_diff" -gt "3" ]; then
  echo "container is unhealthy"
  exit 1
fi

