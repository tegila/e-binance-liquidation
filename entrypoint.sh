#!/bin/bash

# This script subscribes to a MQTT topic using mosquitto_sub.
# On each message received, you can execute whatever you want.
APP="liquidations"
POOL="${APP}..."

DEFAULTMQTT_HOST="c1"
DEFAULTMQTT_TOPIC="futures/binance/btc/usdt/${APP}"

MQTT_HOST="${MQTT_HOST:-$DEFAULTMQTT_HOST}"
MQTT_TOPIC="${MQTT_TOPIC:-$DEFAULTMQTT_TOPIC}"

printf "`date "+%Y-%m-%d %H:%M:%S"` "
echo $MQTT_HOST $MQTT_TOPIC

[ ! -f $POOL ] && mkfifo ${POOL}
(tail -f $POOL | mosquitto_pub -q 2 -l -h ${MQTT_HOST} --topic "${MQTT_TOPIC}/${APP}") &

while true  # Keep an infinite loop to reconnect when connection lost/broker unavailable
do
    node index.js | while read -r payload
    do
        # Here is the callback to execute whenever you receive a message:
        printf "`date "+%Y-%m-%d %H:%M:%S"` "
        # echo "Tx MQTT: ${payload}"

        # break into array
        payload_array=( ${payload} )
        price=${payload_array[0]}
        volume_in=${payload_array[1]}
        volume_out=${payload_array[2]}
        
        printf "$ ${price} \\u20bf ${volume_in} $ ${volume_out} \n"

        echo $volume_out >> ${POOL}
    done
    sleep 3  # Wait 10 seconds until reconnection
done # &  # Discomment the & to run in background (but you should rather run THIS script in background)
