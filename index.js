const ws = require('ws')
// wss://fstream.binance.com/stream?streams=btcusdt@forceOrder
const TICKER = process.env.TICKER || 'btcusdt';
const w = new ws(`wss://fstream.binance.com/stream?streams=${TICKER}@forceOrder`)

let last_liquidation = 0;
w.on('message', (msg) => {
  const obj = JSON.parse(msg);
  if(obj.E <= last_liquidation) return;
  last_liquidation = obj.E;
  const { S, o, q, p, ap, X, T } = obj.data.o;
  // console.log(`S: ${S} o: ${o} p: ${p} q: ${q} ${T} ${X}`)
  const price = Number(p);
  const volume_in = Number(q);
  const volume_out = (S === 'BUY') ? price*volume_in : -1*price*volume_in;
  console.log(`${price} ${volume_in} ${volume_out}`);
});

