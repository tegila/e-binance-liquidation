FROM node:alpine
RUN apk add --no-cache mosquitto-clients bash

WORKDIR /usr/src/binance/spot
COPY package.json .
RUN yarn install
COPY . .

HEALTHCHECK --interval=30s --timeout=5s --start-period=30s \  
    CMD /usr/src/binance/spot/healthcheck.sh

CMD /usr/src/binance/spot/entrypoint.sh | tee output.log
